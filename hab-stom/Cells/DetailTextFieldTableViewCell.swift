//
//  DetailTextFieldTableViewCell.swift
//  hab-stom
//
//  Created by Hadevs on 04/11/2017.
//  Copyright © 2017 Hadevs. All rights reserved.
//

import UIKit

class DetailTextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var label:    UILabel!
    @IBOutlet weak var textView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textView.layer.borderColor = UIColor.black.cgColor
        textView.layer.borderWidth = 0.8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
