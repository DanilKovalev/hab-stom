//
//  ImageCollectionViewCell.swift
//  hab-stom
//
//  Created by Hadevs on 04/11/2017.
//  Copyright © 2017 Hadevs. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
