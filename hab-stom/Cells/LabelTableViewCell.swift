//
//  LabelCollectionViewCell.swift
//  hab-stom
//
//  Created by Hadevs on 04/11/2017.
//  Copyright © 2017 Hadevs. All rights reserved.
//

import UIKit

class LabelTableViewCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
