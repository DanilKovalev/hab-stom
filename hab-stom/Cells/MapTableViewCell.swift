//
//  MapTableViewCell.swift
//  hab-stom
//
//  Created by Hadevs on 06/11/2017.
//  Copyright © 2017 Hadevs. All rights reserved.
//

import UIKit

class MapTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(by item: MapItem) {
        titleLabel.text = item.street
        phoneLabel.text = "тел: \(item.phone)"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
