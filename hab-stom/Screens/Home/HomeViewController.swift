//
//  HomeViewController.swift
//  hab-stom
//
//  Created by Hadevs on 04/11/2017.
//  Copyright © 2017 Hadevs. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITabBarControllerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var items: [UIImage] {
        return [
         #imageLiteral(resourceName: "r1"), #imageLiteral(resourceName: "r2"), #imageLiteral(resourceName: "r3"), #imageLiteral(resourceName: "r4"), #imageLiteral(resourceName: "r5"), #imageLiteral(resourceName: "r6")
        ]
    }
    
    var titles: [String] {
        return [
            "Лечение зубов", "Хирургическая стоматология", "Имплантация", "Исправление вкуса", "Протезирование", "Диагностика и консультации"
        ]
    }
    
    var urls: [URL] {
        var urls:[URL] = []
        for i in 1...6 {
            urls.append(URL.init(string: "http://hab-stom.ru/ios/\(i).html")!)
        }
        return urls
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegating()
        (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.sectionHeadersPinToVisibleBounds = true
        tabBarController?.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let vcs = tabBarController.viewControllers,
              let index = vcs.index(of: viewController) else {
            return false
        }
        
        if index == 1 {
            let url = URL.init(string: "tel://+74212333232")!
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
        return index != 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
    private func delegating() {
        collectionView.delegate  = self
        collectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let url = urls[indexPath.row]
        
        let vc = WebViewController()
        vc.url = url
        vc.title = titles[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header", for: indexPath)
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        let height = 375/667 * view.frame.height
        return CGSize.init(width: view.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeConstant: CGFloat = view.frame.width * 0.8 / 2
        return CGSize.init(width: sizeConstant, height: sizeConstant)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCollectionViewCell
        cell.imageView.image = items[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
