//
//  MapViewController.swift
//  hab-stom
//
//  Created by Hadevs on 06/11/2017.
//  Copyright © 2017 Hadevs. All rights reserved.
//

import UIKit
import MapKit

struct MapItem {
    var street: String
    var phone: String
    
    init(_ street: String, _ phone: String) {
        self.street = street
        self.phone = phone
    }
}

class MapViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    
    private var items: [MapItem] {
        return [
            MapItem.init("ул. Большая 93", "+ 7 (4212) 33-32-32"),
            MapItem.init("ул. Пушкина 4",  "+ 7 (4212) 33-32-32"),
            MapItem.init("ул. Вахова 2",  "+ 7 (4212) 33-32-32")
        ]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tableView.delegate = self
        tableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mapCell", for: indexPath) as! MapTableViewCell
        cell.configure(by: items[indexPath.row])
        cell.separatorInset = .zero
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
